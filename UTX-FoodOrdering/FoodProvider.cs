﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTX_FoodOrdering
{
    public class FoodProvider
    {
        public string Name { get; set; }
        public ObservableCollection<FoodMenu> MenuItems;
    }
}
