﻿namespace UTX_FoodOrdering
{
    public class Order
    {
        public string Name { get; set; }
        public string Provider { get; set; }
        public string Item { get; set; } 
        public decimal Price { get; set; }
    }
}