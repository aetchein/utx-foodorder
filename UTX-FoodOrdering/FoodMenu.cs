﻿namespace UTX_FoodOrdering
{
    public class FoodMenu
    {
        public string FoodName { get; set; }
        public decimal Price { get; set; }
    }
}