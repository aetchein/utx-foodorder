﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace UTX_FoodOrdering
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public FoodApp app = new FoodApp();

        public MainWindow()
        {
            InitializeComponent();
            MyDataGrid.ItemsSource = app.Orders;
            cmbFoodProviders.ItemsSource = app.FoodProviders;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //update view with menu items from selected food provider
            var selectedProvider = (FoodProvider)cmbFoodProviders.SelectedItem;
            MenuGrid.ItemsSource = selectedProvider.MenuItems;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var selectedProvider = (FoodProvider)cmbFoodProviders.SelectedItem;
            if (selectedProvider == null) return;

            var selectedMenuItem = (FoodMenu)MenuGrid.SelectedItem;
            if (selectedMenuItem == null) return;

            app.AddOrder(txtFirstName.Text, txtLastName.Text, selectedProvider, selectedMenuItem);
            txtTotal.Text = app.GetTotal().ToString();
        }

        private void btnApplyFilter_Click(object sender, RoutedEventArgs e)
        {
            decimal.TryParse(txtFilterMin.Text, out decimal minFilter);
            decimal.TryParse(txtFilterMax.Text, out decimal maxFilter);

            // Collection which will take your ObservableCollection
            var _itemSourceList = new CollectionViewSource() { Source = app.Orders };

            // ICollectionView the View/UI part 
            ICollectionView Itemlist = _itemSourceList.View;

            // your Filter
            var filteredRecords = new Predicate<object>(item => ((Order)item).Price > minFilter && ((Order)item).Price < maxFilter);

            //now we add our Filter
            Itemlist.Filter = filteredRecords;

            MyDataGrid.ItemsSource = Itemlist;
        }

        private void btnResetFilter_Click(object sender, RoutedEventArgs e)
        {
            MyDataGrid.ItemsSource = app.Orders;
        }
    }
}
