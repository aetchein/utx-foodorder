﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace UTX_FoodOrdering
{
    public class FoodApp
    {
        public ObservableCollection<Order> Orders = new ObservableCollection<Order>();
        public ObservableCollection<FoodProvider> FoodProviders = new ObservableCollection<FoodProvider>();

        public FoodApp() {
            InitializeMenu();
        }

        private void InitializeMenu()
        {
            var restaurant1 = new FoodProvider
            {
                Name = "Pizza Hut",
                MenuItems = new ObservableCollection<FoodMenu>{
                    new FoodMenu{ FoodName = "Pizza",  Price = 15 },
                    new FoodMenu{ FoodName = "Hamburger",  Price = 6 },
                    new FoodMenu{ FoodName = "Spaghetti",  Price = 8 },
                    new FoodMenu{ FoodName = "Chicken wings",  Price = 6 },
                    new FoodMenu{ FoodName = "Salad",  Price = 4 }
                }
            };

            var restaurant2 = new FoodProvider
            {
                Name = "McD",
                MenuItems = new ObservableCollection<FoodMenu>{
                    new FoodMenu{ FoodName = "Hamburger",  Price = 2 },
                    new FoodMenu{ FoodName = "Salad",  Price = 3 }
                }
            };

            FoodProviders.Add(restaurant1);
            FoodProviders.Add(restaurant2);

            // Add any initialization after the InitializeComponent() call.
            //for (var i = 1; i <= 10; i++)
            //{
            //    Order o = new Order
            //    {
            //        Name = "Full Name " + i.ToString(),
            //        Item = "item " + i.ToString(),
            //        Price = i,
            //        Provider = "sdf"

            //    };
            //    Orders.Add(o);
            //}
        }

        

        internal void AddOrder(string firstName, string lastName, FoodProvider selectedProvider, FoodMenu selectedMenuItem)
        {
            Order o = new Order
            {
                Name = firstName + " " + lastName,
                Item = selectedMenuItem.FoodName,
                Price = selectedMenuItem.Price,
                Provider = selectedProvider.Name
            };

            Orders.Add(o);
        }

        internal decimal GetTotal()
        {
            return Orders.Sum(o => o.Price);
        }
    }
}
